BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start ">
                            <a href="<?php echo site_url('home'); ?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-check"></i>
                                <span class="title">Pendataan</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  <?php if($cluster == 2 || $cluster == 3 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('pendataan/sd'); ?>" class="nav-link ">
                                        <span class="title">Tambah Data SD</span>
                                    </a>
                                </li>
                                <li class="nav-item  <?php if($cluster == 1 || $cluster == 3 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('pendataan/smp'); ?>" class="nav-link ">
                                        <span class="title">Tambah Data SMP</span>
                                    </a>
                                </li>
                                <li class="nav-item  <?php if($cluster == 1 || $cluster == 2 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('pendataan/sma'); ?>" class="nav-link ">
                                        <span class="title">Tambah Data SMA</span>
                                    </a>
                                </li>
                                <li class="nav-item  <?php if($cluster == 1 || $cluster == 2 || $cluster == 3) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('pendataan/smk'); ?>" class="nav-link ">
                                        <span class="title">Tambah Data SMK</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
						<li class="nav-item <?php if($privilege == 3) echo 'hide'; ?>">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-note"></i>
								<span class="title">Koreksi Data</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
                            <li class="nav-item <?php if($cluster == 2 || $cluster == 3 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('koreksi/sd'); ?>" class="nav-link ">
                                        <span class="title">Koreksi Data SD</span>
                                    </a>
                                </li>
								<li class="nav-item <?php if($cluster == 1 || $cluster == 3 || $cluster == 4) echo 'hide'; ?>">
									<a href="<?php echo site_url('koreksi/smp'); ?>" class="nav-link ">
										<span class="title">Koreksi Data SMP</span>
									</a>
								</li>
								<li class="nav-item <?php if($cluster == 1 || $cluster == 2 || $cluster == 4) echo 'hide'; ?>">
									<a href="<?php echo site_url('koreksi/sma'); ?>" class="nav-link ">
										<span class="title">Koreksi Data SMA</span>
									</a>
								</li>
								<li class="nav-item <?php if($cluster == 1 || $cluster == 2 || $cluster == 3) echo 'hide'; ?>">
									<a href="<?php echo site_url('koreksi/smk'); ?>" class="nav-link ">
										<span class="title">Koreksi Data SMK</span>
									</a>
								</li>
							</ul>
						</li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-graph"></i>
                                <span class="title">Laporan Data</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo site_url('cetak/cetak_op'); ?>" class="nav-link ">
                                        <span class="title">Cetak Pendaftaran</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cluster == 2 || $cluster == 3 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('rekap/rekap_data') ?>" class="nav-link ">
                                        <span class="title">Rekap Pendaftaran SD</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cluster == 1 || $cluster == 3 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('rekap/rekap_data') ?>" class="nav-link ">
                                        <span class="title">Rekap Pendaftaran SMP</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cluster == 1 || $cluster == 2 || $cluster == 4) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('rekap/rekap_data') ?>" class="nav-link">
                                        <span class="title">Rekap Pendaftaran SMA</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cluster == 1 || $cluster == 2 || $cluster == 3) echo 'hide'; ?>">
                                    <a href="<?php echo site_url('rekap/rekap_data') ?>" class="nav-link">
                                        <span class="title">Rekap Pendaftaran SMK</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR