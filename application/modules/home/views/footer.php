        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Hak Cipta Dilindungi.
                
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <script type="text/javascript">
            $('.date-picker').datepicker({
                format : 'yyyy-mm-dd',
                autoclose : true
            });
        </script>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../resource/assets/global/plugins/respond.min.js"></script>
<script src="../resource/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        
    </body>
</html>