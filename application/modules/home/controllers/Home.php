<?php
	require_once( APPPATH . 'modules/application_base/controllers/Application_Base.php' );
	
	class Home extends Application_Base
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->load->library('session');
			$this->load->model('M_Login');
		}
		
		public function index()
		{
			$this->load_view();
		}


	}
?>