<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
       
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
						<div class="alert alert-block alert-danger fade in" <?php echo (!isset($error) ? 'hidden' : '') ?>>
							<button type="button" class="close" data-dismiss="alert"></button>
							<p><?php echo (isset($error) ? $error : ''); ?></p>
						</div>
                        <div class="caption font-green-haze">
                            <span class="caption-subject bold uppercase">Cetak Pendaftaran Siswa</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form role="form" class="form-horizontal" method="post" action="<?php echo site_url('cetak/cetak_op'); ?>">
                            <div class="form-body">
                            	<div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Pencarian</label>
                                    <div class="col-md-8">
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-control">
                                                <input type="text" name="no_daftar" class="form-control input-sm" placeholder="(Input No. Daftar) Format : **-***-***-****">
                                                <div class="form-control-focus"> </div>
                                            </div>
                                            <span class="input-group-btn btn-right">
                                                <button class="btn green-haze" type="submit" name="cari_siswa" value="data_cetak">Cari Siswa</button>
                                            </span>
                                		</div>
                                	</div>
                                </div>
                                <hr />
                                
                            </div>
                        </form>
                        <!-- /.modal -->
                        <div class="modal" id="large" tabindex="-1" role="dialog" aria-hidden="false">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="false"></button>
                                        <h4 class="modal-title">&nbsp;</h4>
                                    </div>
                                    <div class="modal-body">
                                    	<div class="invoice-content-2 bordered">
					                        <div class="row invoice-head">
					                            <div class="col-md-5 col-xs-5">
					                                <div class="invoice-logo">
					                                    <img src="" class="img-responsive" alt="Logo Kab./Kota" />
					                                    <p>Lembar Untuk Sekolah</p>
					                                </div>
					                            </div>
					                            <div class="col-md-7 col-xs-7">
					                                <div class="company-address">
					                                    <span class="bold uppercase" style="display: block; font-size: 8pt;"><?php echo(checkNull($nama_institusi,'name'));  ?></span>
					                                    <span style="display: block; font-size: 8pt;">Formulir Pendaftaran <?php echo(checkNull($data_sekolah,'nama'));  ?></span>
					                                    <span style="display: block; font-size: 8pt;">Tahun Pelajaran 2016 / 2017</span>
					                                    <span style="display: block; font-size: 8pt;"><?php echo(checkNull($data_sekolah,'alamat'));?></span>
					                                    <span style="display: block; font-size: 8pt;">http://jelas-ib.id</span>
				                                    </div>
					                            </div>
					                        </div>
					                        <hr />
					                        <div class="row invoice-body">
					                            <div class="col-xs-6 table-responsive">
					                                <table style="width:100%; font-size: 7pt; font-family: Tahoma">
					                                    <tr>
					                                    	<td style="width: 40%">No. Pendaftar</td>
					                                    	<td style="width: 10%">&nbsp;:&nbsp; </td>
					                                    	<td style="width: 50%"><?php echo(checkNull($data_daftar,'no_daftar'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>
					                                    		<?php echo(
					                                    			( $cluster == 1 ? 'No. Akte' : 'No. Ujian' )
					                                    		);?>
				                                    		</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(
					                                    			( $cluster == 1 ? checkNull($data_daftar,'no_akte') : checkNull($data_daftar,'no_ujian') )
					                                    		);?>
				                                    		</td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Nama. Lengkap</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'nama'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Tempat / Tanggal Lahir</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'tmp_lahir'));  ?>, <?php echo(checkNull($data_daftar,'tgl_lahir'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Jenis Kelamin</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo((checkNull($data_daftar,'jenis_kel') == 'L' ? 'Laki-laki' : 'Perempuan'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Alamat</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'alamat'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Telepon</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'no_telp'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Nama Orang Tua</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'nama_ortu'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Asal Sekolah</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'asal_sek'));  ?></td>
					                                    </tr>								                                    
					                                    <tr>
					                                    	<td>Kota / Kabupaten</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'kota_asal_sek'));  ?></td>
					                                    </tr>
					                                </table>
					                            </div>
					                            <div class="col-xs-5 table-responsive">
					                                 <?php
			                                    	$tableNilai = '';
			                                    	if ($cluster != 1) {
			                                    		$tableNilai = (
			                                    			'<table style="width : 100%; font-size: 7pt; font-family: Tahoma; border: 1px solid #000000; padding: 2px;">
							                                    <tr>
							                                    	<th style="width: 20%">No.</th>
							                                    	<th style="width: 60%">Mata Pelajaran</th>
							                                    	<th style="width: 20%">Nilai</th>
							                                    </tr>
							                                    <tr>
							                                    	<td>1</td>
							                                    	<td>Matematika</td>
							                                    	<td>'.checkNull($data_daftar,'nilunmat').'</td>
							                                    </tr>
							                                    <tr>
							                                    	<td>2</td>
							                                    	<td>IPA</td>
							                                    	<td>'.checkNull($data_daftar,'nilunipa').'</td>
							                                    </tr>
							                                    <tr>
							                                    	<td>3</td>
							                                    	<td>Bahasa Indonesia</td>
							                                    	<td>'.checkNull($data_daftar,'nilunbind').'</td>
							                                    </tr>'
		                                    			);
			                                    	} 
			                                    	if ($cluster == 3 || $cluster == 4) {
			                                    		$tableNilai = $tableNilai . 
	                                    					'<tr>
						                                    	<td>3</td>
						                                    	<td>Bahasa Inggris</td>
						                                    	<td>'.checkNull($data_daftar,'nilunbing').'</td>
						                                    </tr>
						                                    </table>&nbsp;';
			                                    	}elseif ($cluster == 2) {
			                                    		$tableNilai = $tableNilai . 
	                                    					'</table>&nbsp;';
			                                    	}
			                                    	echo($tableNilai);
					                                
				                                ?>
					                                <table style="width:100%; font-size: 7pt; font-family: Tahoma; border: 1px solid #000000; padding: 2px;">
					                                    <tr>
					                                    	<th style="width: 20%">Pilihan</th>
					                                    	<th style="width: 40%">Sekolah</th>
															<?php if ($cluster != 1) {
															echo(
					                                    	'<th style="width: 40%">Nilai Akhir</th>'
															);
															}
															 ?>
					                                    </tr>
					                                    <?php 
					                                    	for ($index=0; $index < count($data_pilihan); $index++) {
					                                    		echo (
					                                    			'<tr>
								                                    	<td>'.($index+1).'</td>
								                                    	<td>'.checkNull($data_pilihan[$index], 'nama').'</td>');
																	if ($cluster != 1) {
																	echo (
								                                    	'<td>'.checkNull($data_daftar, 'nilaiakhir').'</td>');
																		}
																	echo (
								                                    '</tr>'
				                                    			);
					                                    	}
					                                    ?>
					                                </table>
					                            </div>
					                        </div>
					                        <hr />
					                        <div class="row invoice-subtotal">
					                            <div class="col-xs-3">
					                                <p class="invoice-title uppercase" style="font-size: 8pt;">Siswa</p>
					                                <br />
					                                <br />
					                                <p class="invoice-desc" style="font-size: 8pt;"><?php echo(strtoupper(checkNull($data_daftar,'nama')));  ?></p>
					                            </div>
					                            <div class="col-xs-3">
					                                <p class="invoice-title uppercase" style="font-size: 8pt;">Orang Tua</p>
					                                <br />
					                                <br />
					                                <p class="invoice-desc" style="font-size: 8pt;"><?php echo(strtoupper(checkNull($data_daftar,'nama_ortu')));  ?></p>
					                            </div>
					                            <div class="col-xs-3">
					                                <p class="invoice-title uppercase" style="font-size: 8pt;">Operator</p>
					                                <br />
					                                <br />
					                                <p class="invoice-desc grand-total" style="font-size: 8pt;"><?php echo(strtoupper(checkNull($nama_operator,'nama')));  ?></p>
					                            </div>
					                        </div>
					                    </div>
					                    <hr style="border:1pt dashed; margin: 10px;"/>
					                    <div class="invoice-content-2 bordered">
					                        <div class="row invoice-head">
					                            <div class="col-md-5 col-xs-5">
					                                <div class="invoice-logo">
					                                    <img src="" class="img-responsive" alt="Logo Kab./Kota" />
					                                    <p>Lembar Untuk Siswa</p>
					                                </div>
					                            </div>
					                            <div class="col-md-7 col-xs-7">
					                                <div class="company-address">
					                                    <span class="bold uppercase" style="display: block; font-size: 8pt;"><?php echo(checkNull($nama_institusi,'name'));  ?></span>
					                                    <span style="display: block; font-size: 8pt;">Formulir Pendaftaran <?php echo(checkNull($data_sekolah,'nama'));  ?></span>
					                                    <span style="display: block; font-size: 8pt;">Tahun Pelajaran 2016 / 2017</span>
					                                    <span style="display: block; font-size: 8pt;"><?php echo(checkNull($data_sekolah,'alamat'));?></span>
					                                    <span style="display: block; font-size: 8pt;">http://jelas-ib.id</span>
				                                    </div>
					                            </div>
					                        </div>
					                        <hr />
					                        <div class="row invoice-body">
					                            <div class="col-xs-6 table-responsive">
					                                <table style="width:100%; font-size: 7pt; font-family: Tahoma">
					                                    <tr>
					                                    	<td style="width: 40%">No. Pendaftar</td>
					                                    	<td style="width: 10%">&nbsp;:&nbsp; </td>
					                                    	<td style="width: 50%"><?php echo(checkNull($data_daftar,'no_daftar'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>
					                                    		<?php 
				                                    			echo(( $cluster == 1 ? 'No. Akte' : 'No. Ujian' ));
					                                    		?>
				                                    		</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td>
					                                    		<?php 
				                                    			echo(( $cluster == 1 ? checkNull($data_daftar,'no_akte') : checkNull($data_daftar,'no_ujian')));
					                                    		?>
				                                    		</td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Nama. Lengkap</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'nama'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Tempat / Tanggal Lahir</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'tmp_lahir'));  ?>, <?php echo(checkNull($data_daftar,'tgl_lahir'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Jenis Kelamin</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo((checkNull($data_daftar,'jenis_kel') == 'L' ? 'Laki-laki' : 'Perempuan'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Alamat</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'alamat'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Telepon</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'no_telp'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Nama Orang Tua</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'nama_ortu'));  ?></td>
					                                    </tr>
					                                    <tr>
					                                    	<td>Asal Sekolah</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'asal_sek'));  ?></td>
					                                    </tr>								                                    
					                                    <tr>
					                                    	<td>Kota / Kabupaten</td>
					                                    	<td>&nbsp;:&nbsp; </td>
					                                    	<td><?php echo(checkNull($data_daftar,'kota_asal_sek'));  ?></td>
					                                    </tr>
					                                </table>
					                            </div>
					                            <div class="col-xs-5 table-responsive">

			                                    <?php
			                                    	$tableNilai = '';
			                                    	if ($cluster != 1) {
			                                    		$tableNilai = (
			                                    			'<table style="width : 100%; font-size: 7pt; font-family: Tahoma; border: 1px solid #000000; padding: 2px;">
							                                    <tr>
							                                    	<th style="width: 20%">No.</th>
							                                    	<th style="width: 60%">Mata Pelajaran</th>
							                                    	<th style="width: 20%">Nilai</th>
							                                    </tr>
							                                    <tr>
							                                    	<td>1</td>
							                                    	<td>Matematika</td>
							                                    	<td>'.checkNull($data_daftar,'nilunmat').'</td>
							                                    </tr>
							                                    <tr>
							                                    	<td>2</td>
							                                    	<td>IPA</td>
							                                    	<td>'.checkNull($data_daftar,'nilunipa').'</td>
							                                    </tr>
							                                    <tr>
							                                    	<td>3</td>
							                                    	<td>Bahasa Indonesia</td>
							                                    	<td>'.checkNull($data_daftar,'nilunbind').'</td>
							                                    </tr>'
		                                    			);
			                                    	} 
			                                    	if ($cluster == 3 || $cluster == 4) {
			                                    		$tableNilai = $tableNilai . 
	                                    					'<tr>
						                                    	<td>3</td>
						                                    	<td>Bahasa Inggris</td>
						                                    	<td>'.checkNull($data_daftar,'nilunbing').'</td>
						                                    </tr>
						                                    </table>&nbsp;';
			                                    	}elseif ($cluster == 2) {
			                                    		$tableNilai = $tableNilai . 
	                                    					'</table>&nbsp;';
			                                    	}
			                                    	echo($tableNilai);
					                                
				                                ?>
					                                <table style="width:100%; font-size: 7pt; font-family: Tahoma; border: 1px solid #000000; padding: 2px;">
					                                    <tr>
					                                    	<th style="width: 20%">Pilihan</th>
					                                    	<th style="width: 40%">Sekolah</th>
					                                    	<?php if ($cluster != 1) {
															echo(
					                                    	'<th style="width: 40%">Nilai Akhir</th>'
															);
															}
															 ?>
					                                    </tr>
					                                    <?php 
					                                    	for ($index=0; $index < count($data_pilihan); $index++) {
					                                    		echo (
					                                    			'<tr>
								                                    	<td>'.($index+1).'</td>
								                                    	<td>'.checkNull($data_pilihan[$index], 'nama').'</td>');
																	if ($cluster != 1) {
																	echo (
								                                    	'<td>'.checkNull($data_daftar, 'nilaiakhir').'</td>');
																		}
																	echo (
								                                    '</tr>'
				                                    			);
					                                    	}
					                                    ?>
					                                </table>
					                            </div>
					                        </div>
					                        <hr />
					                        <div class="row invoice-subtotal">
					                            <div class="col-xs-3">
					                                <p class="invoice-title uppercase" style="font-size: 8pt;">Siswa</p>
					                                <br />
					                                <br />
					                                <p class="invoice-desc" style="font-size: 8pt;"><?php echo(strtoupper(checkNull($data_daftar,'nama')));  ?></p>
					                            </div>
					                            <div class="col-xs-3">
					                                <p class="invoice-title uppercase" style="font-size: 8pt;">Orang Tua</p>
					                                <br />
					                                <br />
					                                <p class="invoice-desc" style="font-size: 8pt;"><?php echo(strtoupper(checkNull($data_daftar,'nama_ortu')));  ?></p>
					                            </div>
					                            <div class="col-xs-3">
					                                <p class="invoice-title uppercase" style="font-size: 8pt;">Operator</p>
					                                <br />
					                                <br />
					                                <p class="invoice-desc grand-total" style="font-size: 8pt;"><?php echo(strtoupper(checkNull($nama_operator,'nama')));  ?></p>
					                            </div>
					                        </div>
					                    </div>
                                    </div>
                                    <div class="modal-footer">
                                    	<div class="col-xs-12">
			                                <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="javascript:window.print();">Print</a>
			                            </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- BEGIN SAMPLE FORM PORTLET-->
                
                <!-- END SAMPLE FORM PORTLET-->
                <!-- BEGIN SAMPLE FORM PORTLET-->
                
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php $this->load->view('js_cetak'); ?>