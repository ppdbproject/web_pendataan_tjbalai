<?php
	require_once( APPPATH . 'modules/application_base/controllers/Application_Base.php' );
	require_once( APPPATH . 'modules/cetak/controllers/features.php' );

	class Cetak extends Application_Base
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->load->library('session');
			$this->load->model('M_Cetak');
			$this->load->model('login/M_Login');
		}

		public function cetak_op()
		{
			$data = array();
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$authority = $this->M_Login->get_authority($username);
			$cluster = ($authority['k_sekolah'] ? substr($authority['k_sekolah'], 0, 1) : 0);

			if($cluster == 1){ $tablePilihan = 'pilihan_sd'; }
			else if($cluster == 2){ $tablePilihan = 'pilihan_smp'; }
			else if($cluster == 3){ $tablePilihan = 'pilihan_sma'; }
			else{ $tablePilihan = 'pilihan_smk'; }

			switch ($this->input->post('cari_siswa')) {
				case 'data_cetak':
					if( $_POST['no_daftar'] != '' ){
						$data['nama_institusi'] = $this->M_Cetak->get_nama_institusi();
						$data['data_sekolah'] = $this->M_Cetak->get_data_sekolah($username);
						$data['data_daftar'] = $this->M_Cetak->get_data_daftar($_POST['no_daftar'], $authority['k_sekolah']);
						$data['nama_operator'] = $this->M_Login->get_op_name($username);
						$data['data_pilihan'] = $this->M_Cetak->get_pilihan($_POST['no_daftar'], $tablePilihan);
						$data['cluster'] = $cluster;
						if ($tablePilihan == 'pilihan_smk') {
							for ($index=0; $index < count($data['data_pilihan']) ; $index++) {
								$data['data_pilihan'][$index] = 
								(isset($data['data_pilihan'][$index]['pilih_sek']) ? $this->M_Cetak->get_nama_sekolah( substr($data['data_pilihan'][$index]['pilih_sek'], 0, 3) ) : '');
							}
							if ( count($data['data_daftar']) == 0 ) {
								$data['error'] = 'Data Siswa Tidak Ditemukan';	
							}
						} 
						else {
							for ($index=0; $index < count($data['data_pilihan']) ; $index++) { 
								$data['data_pilihan'][$index] = 
								(isset($data['data_pilihan'][$index]['pilih_sek']) ? $this->M_Cetak->get_nama_sekolah($data['data_pilihan'][$index]['pilih_sek']) : '');
							}
							if ( count($data['data_daftar']) == 0 ) {
								$data['error'] = 'Data Siswa Tidak Ditemukan';	
							}	
						}
						
					} else {
						$data['error'] = 'Masukkan No Pendaftaran Terlebih Dahulu';
					}
					
					break;
				default:
					# code...
					break;
			}
			
			$this->load_view('Cetak', $data);
		}
	}
?>