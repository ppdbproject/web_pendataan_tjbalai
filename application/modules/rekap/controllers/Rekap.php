<?php
	require_once( APPPATH . 'modules/application_base/controllers/Application_Base.php' );
	require_once( APPPATH . 'modules/rekap/controllers/features.php' );
	
	class Rekap extends Application_Base
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->load->library('session');
			
			$this->load->model('M_Rekap');
			$this->load->model('login/M_login');
		}

		public function rekap_data() {
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$authority = $this->M_login->get_authority($username);

			$cluster = ($authority['k_sekolah'] ? substr($authority['k_sekolah'], 0, 1) : 0);
			$view = '';
			
			if($cluster == 1){ $tablePendaftar = 'pendaftar_sd'; $view = 'sd'; }
			else if($cluster == 2){ $tablePendaftar = 'pendaftar_smp'; $view = 'smp'; }
			else if($cluster == 3){ $tablePendaftar = 'pendaftar_sma'; $view ='sma'; }
			else{ $tablePendaftar = 'pendaftar_smk'; $view = 'smk'; }

			$data=array();
			$data['data_daftar'] = $this->M_Rekap->get_data_daftar($tablePendaftar ,$authority['k_sekolah']);
			$this->load_view($view, $data);
		}
	}
?>