
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-haze">
                            <span class="caption-subject bold uppercase">Rekap Data Pendaftar</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> No </th>
                                    <th> No Daftar </th>
                                    <th> No Akte </th>
                                    <th> Nama </th>
                                    <th> Asal Sekolah </th>
                                    <th> Kota </th>
                                    <th> Waktu Pendaftaran </th>

                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if (count($data_daftar > 0)) {
                                    for ($index=0; $index < count($data_daftar); $index++) { 
                                        echo(
                                            '<tr>
                                                <td>'.($index+1).'</td>
                                                <td>' .$data_daftar[$index]['no_daftar']. '</td>
                                                <td>' .$data_daftar[$index]['no_akte']. '</td>
                                                <td>' .$data_daftar[$index]['nama']. '</td>
                                                <td>' .$data_daftar[$index]['asal_sek']. '</td>
                                                <td>' .$data_daftar[$index]['kota_asal_sek']. '</td>
                                                <td>' .$data_daftar[$index]['waktu_daftar']. '</td>
                                            </tr>'
                                        );
                                    }   
                                }
                                
                            ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->