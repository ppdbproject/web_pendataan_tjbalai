<div class="form-body">
	<div class="form-group form-md-line-input">
		<label class="col-md-2 control-label" for="form_control_1">Pencarian</label>
		<div class="col-md-8">
			<div class="input-group input-group-sm">
				<div class="input-group-control">
					<input type="text" name="no_daftar" class="form-control input-sm"
						placeholder="(Input No. Daftar) Format : **-***-***-****" value="<?php echo checkNull($this->data['siswa'], 'no_daftar') ?>">
					<div class="form-control-focus"> </div>
				</div>
				<span class="input-group-btn btn-right">
					<button class="btn green-haze" type="submit" name="form_koreksi" value="cari_siswa">Cari Siswa</button>
				</span>
			</div>
		</div>
	</div>
</div>