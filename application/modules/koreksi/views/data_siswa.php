<div class="form-group form-md-line-input">
	<label class="col-md-2 control-label" for="form_control_1">No Ujian</label>
	<div class="col-md-4">
		<input type="text" name="no_ujian" value="<?php echo checkNull($this->data['siswa'], 'no_ujian'); ?>" class="form-control"  id="form_control_1" placeholder="No Ujian">
		<div class="form-control-focus"> </div>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">Alamat Rumah</label>
	<div class="col-md-4">
		<input type="text" name="alamat" value="<?php echo checkNull($this->data['siswa'], 'alamat'); ?>" class="form-control" id="form_control_1" placeholder="">
		<div class="form-control-focus"> </div>
	</div>
</div>
<div class="form-group form-md-line-input">
	<label class="col-md-2 control-label" for="form_control_1">Nama Siswa</label>
	<div class="col-md-4">
		<input type="text" name="nama" value="<?php echo checkNull($this->data['siswa'], 'nama'); ?>" class="form-control" id="form_control_1" placeholder="">
		<div class="form-control-focus"> </div>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
		<?php
			$kota = checkNull($this->data['siswa'], 'kota');
			$flag = ($kota == '');
		?>
	<div class="col-md-4">
		<?php
			echo(
				'<div class="md-radio-inline kota">
					<div class="md-radio">
						<input type="radio" name="kota" id="radio57" class="md-radiobtn" value="' . $kota . '" ' . ($kota && $kota != 'other' ? 'checked' : '') . '>
						<label for="radio57">
							<span></span>
							<span class="check"></span>
							<span class="box"></span> ' . $kota . '
						</label>
					</div>
					<div class="md-radio">
						<input type="radio" name="kota" id="radio58" class="md-radiobtn" value="other" ' . ($kota == 'other' ? 'checked' : '') . '>
						<label for="radio58">
							<span></span>
							<span class="check"></span>
							<span class="box"></span> Lainnya </label>
					</div>
				</div>
				<input type="text" name="kota" class="form-control" id="form_control_1" placeholder="">
				<div class="form-control-focus"></div>'
			);
		?>
	</div>
</div>
<div class="form-group form-md-line-input">
	<label class="col-md-2 control-label" for="form_control_1">Jenis Kelamin</label>
	<div class="col-md-4">
		<div class="md-radio-inline">
			<div class="md-radio">
				<input type="radio" name="jenis_kel" id="radio53" class="md-radiobtn" value="L" <?php echo (checkNull($this->data['siswa'], 'jenis_kel') == 'L' ? 'checked' : ''); ?>>
				<label for="radio53">
					<span></span>
					<span class="check"></span>
					<span class="box" ></span> Laki-laki </label>
			</div>
			<div class="md-radio">
				<input type="radio" name="jenis_kel" id="radio54" class="md-radiobtn" value="P" <?php echo (checkNull($this->data['siswa'], 'jenis_kel') == 'P' ? 'checked' : ''); ?>>
				<label for="radio54">
					<span></span>
					<span class="check"></span>
					<span class="box"></span> Perempuan </label>
			</div>
		</div>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">No. Telpon</label>
	<div class="col-md-4">
		<input type="text" name="no_telp" value="<?php echo checkNull($this->data['siswa'], 'no_telp'); ?>" class="form-control" id="form_control_1" placeholder="">
		<div class="form-control-focus"> </div>
	</div>
</div>
<div class="form-group form-md-line-input">
	<label class="col-md-2 control-label" for="form_control_1">Tempat Lahir</label>
	<div class="col-md-4">
		<input type="text" name="tmp_lahir" value="<?php echo checkNull($this->data['siswa'], 'tmp_lahir'); ?>" class="form-control" id="form_control_1" placeholder="">
		<div class="form-control-focus"> </div>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">Nama Orang Tua</label>
	<div class="col-md-4">
		<input type="text" name="nama_ortu" value="<?php echo checkNull($this->data['siswa'], 'nama_ortu'); ?>" class="form-control" id="form_control_1" placeholder="">
		<div class="form-control-focus"> </div>
	</div>
</div>
<div class="form-group form-md-line-input">
	<label class="control-label col-md-2">Tanggal Lahir</label>
	<div class="col-md-4">
		<input class="form-control form-control-inline input-medium date-picker" name="tgl_lahir" value="<?php echo checkNull($this->data['siswa'], 'tgl_lahir'); ?>" size="16" type="text"/>
		<span class="help-block"> Masukkan Tanggal Lahir </span>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">Pilihan 1</label>
	<div class="col-md-4">
		<select name="pilihan1" class="form-control" id="form_control_1">
			<option value="">Pilih Sekolah</option>
			<?php
				for($index = 0;$index<$count;$index++) {
					echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'" ' . ($this->data['list_sekolah'][$index]['k_sekolah'] == $this->data['siswa']['pilihan1'] ? 'selected' : '') . '>' .
						$this->data['list_sekolah'][$index]['nama'] .'</option>');
				}
			?>
		</select>
		<div class="form-control-focus"> </div>
	</div>
</div>
<div class="form-group form-md-line-input">
	<label class="col-md-2 control-label" for="form_control_1">Asal Sekolah</label>
	<div class="col-md-4">
		<input type="text" name="asal_sek" value="<?php echo checkNull($this->data['siswa'], 'asal_sek'); ?>" class="form-control" 	 id="form_control_1" placeholder="">
		<div class="form-control-focus"> </div>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">Pilihan 2</label>
	<div class="col-md-4">
		<select name="pilihan2" class="form-control" id="form_control_1">
			<option value="">Pilih Sekolah</option>
			<?php
				for($index = 0;$index<$count;$index++) {
					echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'" ' . ($this->data['list_sekolah'][$index]['k_sekolah'] == $this->data['siswa']['pilihan2'] ? 'selected' : '') . '>' .
						$this->data['list_sekolah'][$index]['nama'] .'</option>');
				}
			?>
		</select>
		<div class="form-control-focus"> </div>
	</div>
</div>
	<?php
		$kota_asal_sek = checkNull($this->data['siswa'], 'kota_asal_sek');
	?>
<div class="form-group form-md-line-input">
	<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
	<div class="col-md-4">
		<?php
			echo(
				'<div class="md-radio-inline kota_asal_sek">
					<div class="md-radio">
						<input type="radio" name="kota_asal_sek" id="radio55" class="md-radiobtn" value="' . $kota_asal_sek.'" ' . ($kota_asal_sek && $kota_asal_sek != 'other' ? 'checked' : '') . '>
						<label for="radio55">
							<span></span>
							<span class="check"></span>
							<span class="box"></span> ' . $kota_asal_sek . '
						</label>
					</div>
					<div class="md-radio">
						<input type="radio" name="kota_asal_sek" id="radio56" class="md-radiobtn" value="other" ' . ($kota_asal_sek == 'other' ? 'checked' : '') . '>
						<label for="radio56">
							<span></span>
							<span class="check"></span>
							<span class="box"></span> Lainnya </label>
					</div>
				</div>
				<input type="text" name="kota_asal_sek" class="form-control kota_asal_sek_lainnya" id="form_control_1" placeholder="">
				<div class="form-control-focus"> </div>'
			);
		?>
	</div>
	<label class="col-md-2 control-label" for="form_control_1">Pilihan 3</label>
	<div class="col-md-4">
		<select name="pilihan3" class="form-control" id="form_control_1">
			<option value="">Pilih Sekolah</option>
			<?php
				for($index = 0;$index<$count;$index++) {
					echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'" ' . ($this->data['list_sekolah'][$index]['k_sekolah'] == $this->data['siswa']['pilihan3'] ? 'selected' : '') . '>' .
						$this->data['list_sekolah'][$index]['nama'] .'</option>');
				}
			?>
		</select>
		<div class="form-control-focus"> </div>
	</div>
</div>