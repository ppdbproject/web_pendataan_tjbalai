<?php
	$count = count($list_sekolah);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEAD-->
		
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE BREADCRUMB -->
		
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE BASE CONTENT -->
	   
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet light bordered">
					<div class="alert alert-block alert-success fade in" <?php echo (!isset($notification) ? 'hidden' : '') ?>>
						<button type="button" class="close" data-dismiss="alert"></button>
						<p><?php echo (isset($notification) ? $notification : ''); ?></p>
					</div>
					<div class="alert alert-block alert-danger fade in" <?php echo (!isset($error) ? 'hidden' : '') ?>>
						<button type="button" class="close" data-dismiss="alert"></button>
						<p><?php echo (isset($error) ? $error : ''); ?></p>
					</div>
					<div class="portlet-title">
						<div class="caption font-green-haze">
							<span class="caption-subject bold uppercase">Koreksi Data Siswa</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" method="post" action="<?php echo site_url('koreksi/sma'); ?>">
							<?php include('pencarian.php') ?>
							<hr />
							<div class="form-body">
								<?php include('data_siswa.php') ?>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN Bhs. Inggris</label>
									<div class="col-md-4">
										<input type="text" name="nilunbing" value="<?php echo checkNull($this->data['siswa'], 'nilunbing'); ?>" class="form-control"  id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN Matematika</label>
									<div class="col-md-4">
										<input type="text" name="nilunmat" value="<?php echo checkNull($this->data['siswa'], 'nilunmat'); ?>" class="form-control"  id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN IPA</label>
									<div class="col-md-4">
										<input type="text" name="nilunipa" value="<?php echo checkNull($this->data['siswa'], 'nilunipa'); ?>" class="form-control"  id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN Bahasa Indonesia</label>
									<div class="col-md-4">
										<input type="text" name="nilunbind" value="<?php echo checkNull($this->data['siswa'], 'nilunbind'); ?>" class="form-control"  id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-12">
										<button type="submit" class="btn blue" name="form_koreksi" value="update_cetak_siswa">Koreksi dan Print</button>
										<button type="submit" class="btn blue" name="form_koreksi" value="update_siswa" >Koreksi Siswa</button>
										<button type="button" class="btn blue" name="form_koreksi" onclick="cancelKoreksi();">Batal</button>
										<button type="submit" class="btn blue" name="form_koreksi" value="delete_siswa" >Hapus Pendaftaran</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		
		<!-- END PAGE BASE CONTENT -->
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php $this->load->view('javascript_koreksi_data'); ?>