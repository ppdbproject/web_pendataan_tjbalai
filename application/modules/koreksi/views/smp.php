<?php
	$count = count($this->data['list_sekolah']);
	$kota = checkNull($this->data['institution'], 'kota');
	$kotaSiswa = checkNull($this->data['siswa'], 'kota');
	$kotaSekolahSiswa = checkNull($this->data['siswa'], 'kota_asal_sek');
	$flag = ($kota == '');
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEAD-->
		
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE BREADCRUMB -->
		
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE BASE CONTENT -->
	   
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet light bordered">
					<?php include(APPPATH . 'modules/pendataan/views/notification.php') ?>
					<div class="portlet-title">
						<div class="caption font-green-haze">
							<span class="caption-subject bold uppercase">Formulir Pendaftaran Siswa</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" method="post" action="<?php echo site_url('koreksi/smp'); ?>">
							<div class="form-body">
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Pencarian</label>
									<div class="col-md-8">
										<div class="input-group input-group-sm">
											<div class="input-group-control">
												<input type="text" name="no_daftar" class="form-control"
													placeholder="(Input No. Daftar) Format : **-***-***-****" value="<?php echo checkNull($this->data['siswa'], 'no_daftar') ?>">
												<div class="form-control-focus"> </div>
											</div>
											<span class="input-group-btn btn-right">
												<button class="btn green-haze" type="submit" name="form_koreksi" value="cari_siswa">Cari Siswa</button>
											</span>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-body">
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/nama.php') ?>
									<?php include(APPPATH . 'modules/pendataan/views/alamat.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/jenis_kelamin.php') ?>
									<?php include(APPPATH . 'modules/pendataan/views/kota.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/tempat_lahir.php') ?>
									<?php include(APPPATH . 'modules/pendataan/views/no_telp.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/tgl_lahir.php') ?>
									<?php include(APPPATH . 'modules/pendataan/views/nama_ortu.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/asal_sekolah.php') ?>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 1</label>
									<div class="col-md-4">
										<select name="pilihan1" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
											<?php
												for($index = 0;$index<$count;$index++) {
													echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'" ' . ($this->data['list_sekolah'][$index]['k_sekolah'] == $this->data['siswa']['pilihan1'] ? 'selected' : '') . '>' .
														$this->data['list_sekolah'][$index]['nama'] .'</option>');
												}
											?>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/kota_asal_sekolah.php') ?>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 2</label>
									<div class="col-md-4">
										<select name="pilihan2" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
											<?php
												for($index = 0;$index<$count;$index++) {
													echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'" ' . ($this->data['list_sekolah'][$index]['k_sekolah'] == $this->data['siswa']['pilihan2'] ? 'selected' : '') . '>' .
														$this->data['list_sekolah'][$index]['nama'] .'</option>');
												}
											?>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/un_bhs_ind.php') ?>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 3</label>
									<div class="col-md-4">
										<select name="pilihan3" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
											<?php
												for($index = 0;$index<$count;$index++) {
													echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'" ' . ($this->data['list_sekolah'][$index]['k_sekolah'] == $this->data['siswa']['pilihan3'] ? 'selected' : '') . '>' .
														$this->data['list_sekolah'][$index]['nama'] .'</option>');
												}
											?>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/un_mat.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include(APPPATH . 'modules/pendataan/views/un_ipa.php') ?>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-12">
										<button type="submit" class="btn blue" name="form_koreksi" value="update_cetak_siswa">Koreksi dan Print</button>
										<button type="submit" class="btn blue" name="form_koreksi" value="update_siswa" >Koreksi Siswa</button>
										<button type="button" class="btn blue" name="form_koreksi" onclick="cancelKoreksi();">Batal</button>
										<button type="submit" class="btn blue" name="form_koreksi" value="delete_siswa" >Hapus Pendaftaran</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		
		<!-- END PAGE BASE CONTENT -->
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include(APPPATH . 'modules/pendataan/views/javascript_pendataan.php') ?>
<?php include('javascript_koreksi_data.php') ?>