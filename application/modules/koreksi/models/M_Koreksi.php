<?php
	class M_Koreksi extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		
		function get_data_siswa($noDaftar, $tableName) {
			$sql = "SELECT * FROM $tableName
				WHERE `no_daftar` LIKE '$noDaftar' AND
					(is_hapus <> '1' OR is_hapus IS NULL)";
					
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}
		
		function get_pilihan($noDaftar, $tableName) {
			$sql = "SELECT * FROM $tableName
				WHERE `no_daftar` LIKE '$noDaftar'";
				
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->result_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}
		
		function insert($tableName, $data)
		{
			return $this->db->insert($tableName, $data);
		}
		
		function update($tableName, $data, $where)
		{
			$this->db->where($where);
			return $this->db->update($tableName, $data);
		}
		
		function delete($tableName, $noDaftar)
		{
			$sql = "UPDATE $tableName
				SET is_hapus = 1
				WHERE `no_daftar` LIKE '$noDaftar'";
			
			return $this->db->query($sql);
		}
	}	
?>