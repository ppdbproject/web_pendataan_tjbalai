<?php
	require_once( APPPATH . 'modules/application_base/controllers/Application_Base.php' );
	require_once( APPPATH . 'modules/pendataan/controllers/features.php' );
	require_once( APPPATH . 'modules/pendataan/controllers/Pendataan.php' );

	class Koreksi extends Application_Base
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->load->library('session');
			
			$this->load->model('M_Koreksi');
			$this->load->model('login/M_login');
			$this->load->model('M_Pendataan');
		}
		
		public function sd()
		{
			$this->koreksiProcess(CLUSTER_SD);
		}
		
		public function smp()
		{
			$this->koreksiProcess(CLUSTER_SMP);
		}
		
		public function sma()
		{
			$this->koreksiProcess(CLUSTER_SMA);			
		}
		
		public function smk()
		{
			$this->koreksiProcess(CLUSTER_SMK);
		}
		
		public function getPilihan($cluster)
		{
			for($index = 1; $index <= 3; $index++)
			{
				$data['pilihan' . $index] = array(
					'pilih_urut' => $index,
					'pilih_sek' => ($cluster != CLUSTER_SMK ? $this->input->post('pilihan' . $index) : $this->input->post('jurusan' . $index))
				);
			}
			
			return $data;
		}
		
		private function search($cluster, $tabelPendaftar, $tabelPilihan)
		{
			$noDaftar = $this->input->post('no_daftar');
			if($noDaftar)
			{
				$data['siswa'] = $this->M_Koreksi->get_data_siswa($noDaftar, $tabelPendaftar);
				if($data['siswa'])
				{
					$pilihan = $this->M_Koreksi->get_pilihan($noDaftar, $tabelPilihan);
					
					$data['siswa']['pilihan1'] = (isset($pilihan[0]['pilih_sek']) ? substr($pilihan[0]['pilih_sek'], 0, 3) : '');
					$data['siswa']['pilihan2'] = (isset($pilihan[1]['pilih_sek']) ? substr($pilihan[1]['pilih_sek'], 0, 3) : '');
					$data['siswa']['pilihan3'] = (isset($pilihan[2]['pilih_sek']) ? substr($pilihan[2]['pilih_sek'], 0, 3) : '');
					if($cluster == CLUSTER_SMK)
					{
						$data['siswa']['jurusan1'] = (isset($pilihan[0]['pilih_sek']) ? $pilihan[0]['pilih_sek'] : '');
						$data['siswa']['jurusan2'] = (isset($pilihan[1]['pilih_sek']) ? $pilihan[1]['pilih_sek'] : '');
						$data['siswa']['jurusan3'] = (isset($pilihan[2]['pilih_sek']) ? $pilihan[2]['pilih_sek'] : '');
					}
				}
				else
				{
					$data['error'] = 'Nomor pendaftaran tidak ditemukan';
				}
			}
			else
			{
				$data['error'] = 'Nomor daftar perlu diisi dahulu';
			}
			
			return $data;
		}
		
		private function koreksiProcess($cluster)
		{
			$this->load->model('M_Cetak');
			
			$clusterPendaftar = $this->getClusterPendaftar($cluster);
			
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$authority = $this->M_login->get_authority($username);
			
			switch($this->input->post('form_koreksi'))
			{
				case 'cari_siswa' :
					$data = $this->search($cluster, $clusterPendaftar['tabel_pendaftar'], $clusterPendaftar['tabel_pilihan']);
					
					break;
					
				case 'update_siswa' :
					$data = $this->updatePendaftaran($cluster, $clusterPendaftar['tabel_pendaftar'], $clusterPendaftar['tabel_pilihan']);
					if(isset($data['notification']) && $data['notification'])
					{
						$data['siswa'] = array();
					}
					
					break;
					
				case 'update_cetak_siswa' :
					$data = $this->updatePendaftaran($cluster, $clusterPendaftar['tabel_pendaftar'], $clusterPendaftar['tabel_pilihan']);
					if(
						(isset($data['error']) && !$data['error']) ||
						(isset($data['notification']) && $data['notification'])
					)
					{
						$data['nama_institusi'] = $this->M_Cetak->get_nama_institusi();
						$data['data_sekolah'] = $this->M_Cetak->get_data_sekolah($username);
						$data['data_daftar'] = $this->M_Cetak->get_data_daftar($data['siswa']['no_daftar'], $authority['k_sekolah']);
						$data['nama_operator']['nama'] = $session['operator_name'];
						for($index = 0; $index < 3; $index++)
						{
							if($data['siswa']['pilihan' . ($index + 1)]){
								$tmp = $this->M_Cetak->get_nama_sekolah( substr($data['siswa']['pilihan' . ($index + 1)], 0, 3) );
								$data['data_pilihan'][$index]['nama'] = $tmp['nama'];
							}
						}
						
						$clusterPendaftar['view'] = 'cetak/cetak';
					}
					
					break;
					
				case 'delete_siswa' :
					$data = $this->deletePendaftaran($cluster, $clusterPendaftar['tabel_pendaftar'], $clusterPendaftar['tabel_pilihan']);
					if(isset($data['notification']) && $data['notification'])
					{
						$data['siswa'] = array();
					}
					
					break;
			}
			
			if(!isset($data['siswa']))
			{
				$data['siswa'] = array();
			}
			
			$data['list_sekolah'] = $this->M_Pendataan->get_data_sekolah($username);
			$data['institution']['kota'] = $this->cityValue;
			
			$this->load_view($clusterPendaftar['view'], $data);
		}
		
		private function getClusterPendaftar($cluster)
		{
			switch($cluster)
			{
				case CLUSTER_SD :
					$result = array(
						'view' => 'sd',
						'tabel_pendaftar' => 'pendaftar_sd',
						'tabel_pilihan' => 'pilihan_sd'
					);					
					return $result;
					
				case CLUSTER_SMP :
					$result = array(
						'view' => 'smp',
						'tabel_pendaftar' => 'pendaftar_smp',
						'tabel_pilihan' => 'pilihan_smp'
					);					
					return $result;
					
				case CLUSTER_SMA :
					$result = array(
						'view' => 'sma',
						'tabel_pendaftar' => 'pendaftar_sma',
						'tabel_pilihan' => 'pilihan_sma'
					);					
					return $result;
					
				case CLUSTER_SMK :
					$result = array(
						'view' => 'smk',
						'tabel_pendaftar' => 'pendaftar_smk',
						'tabel_pilihan' => 'pilihan_smk'
					);					
					return $result;
			}
		}
		
		private function updatePendaftaran($cluster, $tabelPendaftar, $tabelPilihan)
		{
			$data = $this->search($cluster, $tabelPendaftar, $tabelPilihan);
			if(isset($data['siswa']) && $data['siswa'])
			{
				$data['siswa']['alamat'] = $this->input->post('alamat');
				$data['siswa']['nama'] = $this->input->post('nama');
				$data['siswa']['kota'] = ($this->input->post('kota') ? $this->input->post('kota') : $this->input->post('input_kota'));
				$data['siswa']['jenis_kel'] = $this->input->post('jenis_kel');
				$data['siswa']['no_telp'] = $this->input->post('no_telp');
				$data['siswa']['tmp_lahir'] = $this->input->post('tmp_lahir');
				$data['siswa']['nama_ortu'] = $this->input->post('nama_ortu');
				$data['siswa']['tgl_lahir'] = $this->input->post('tgl_lahir');
				
				$dataUpdate = array(
					'alamat' => $data['siswa']['alamat'],
					'nama' => $data['siswa']['nama'],
					'kota' => $data['siswa']['kota'],
					'jenis_kel' => $data['siswa']['jenis_kel'],
					'no_telp' => $data['siswa']['no_telp'],
					'tmp_lahir' => $data['siswa']['tmp_lahir'],
					'nama_ortu' => $data['siswa']['nama_ortu'],
					'tgl_lahir' => $data['siswa']['tgl_lahir'],
				);
				
				$update = $this->M_Koreksi->update($tabelPendaftar, $dataUpdate, array('no_daftar' => $data['siswa']['no_daftar']));
				if($update)
				{
					for($index = 1;$index <= 3; $index++)
					{
						if($data['siswa']['pilihan' . $index]){
							$update = $this->M_Koreksi->update($tabelPilihan,
								array(
									'pilih_sek' => ($cluster != CLUSTER_SMK ? $this->input->post('pilihan' . $index) : $this->input->post('jurusan' . $index))
								),
								array(
									'no_daftar' => $data['siswa']['no_daftar'],
									'pilih_sek' => $data['siswa']['pilihan' . $index],
									'pilih_urut' => $index
								)
							);
						}
						else
						{
							$dataPilihan = array(
								'no_daftar' => $data['siswa']['no_daftar'],
								'pilih_urut' => $index,
								'pilih_sek' => ($cluster != CLUSTER_SMK ? $this->input->post('pilihan' . $index) : $this->input->post('jurusan' . $index))
							);
							
							if($cluster != CLUSTER_SD)
							{
								$dataPilihan['nilaiakhir'] = $data['siswa']['nilaiakhir'];
							}
							
							$update = $this->M_Koreksi->insert($tabelPilihan, $dataPilihan);
						}
						
						if($update)
						{
							$data['notification'] = 'Koreksi siswa berhasil';
							$data['siswa']['pilihan' . $index] = ($cluster != CLUSTER_SMK ? $this->input->post('pilihan' . $index) : $this->input->post('jurusan' . $index));
						}
						else
						{
							$data['error'] = 'Koreksi siswa gagal';
						}
					}
				}
				else
				{
					$data['error'] = 'Koreksi siswa gagal';
				}
			}
			
			return $data;
		}
		
		private function deletePendaftaran($cluster, $tabelPendaftar, $tabelPilihan)
		{
			$data = $this->search($cluster, $tabelPendaftar, $tabelPilihan);
			if(isset($data['siswa']) && $data['siswa'])
			{
				$delete = $this->M_Koreksi->delete($tabelPendaftar, $data['siswa']['no_daftar']);
				if($delete)
				{
					$data['notification'] = 'Hapus siswa berhasil';
					$data['siswa'] = array();
				}
				else
				{
					$data['error'] = 'Hapus siswa gagal';
				}
			}
			
			return $data;
		}
	}
?>