<?php
	class Login extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$this->load->model('M_Login');
		}
		
		public function index()
		{	
			$this->load->view('login', array());
		}
		
		public function loginProcess(){
			$username = $this->input->post('username');
			$password = sha1($this->config->item('encryption_key') . $this->input->post('password'));
			
			$result = $this->M_Login->login_process($username, $password);
			if($result)
			{
				if ( $result['is_blokir'] ==0 ) {
					$this->session->set_userdata(array(
						'username' => $result['opr_id'],
						'operator_name' => $result['nama'],
						'privilege' => $result['id_role'],
						'cluster' => $result['k_sekolah']
					));
					redirect(site_url('home'));
				} else {
					$data['error'] = 'Akses Diblokir';
					$this->load->view('login', $data);	
				}
			}else
			{
				$data['error'] = 'Username / Password Salah';
				$this->load->view('login', $data);
			}
		}
		
		public function logoutProcess()
		{
			$this->session->sess_destroy();
			redirect(site_url('login'));
		}
	}
?>