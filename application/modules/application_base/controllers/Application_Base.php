<?php

	define('CLUSTER_ALL'	, 0);
	define('CLUSTER_SD'		, 1);
	define('CLUSTER_SMP'	, 2);
	define('CLUSTER_SMA'	, 3);
	define('CLUSTER_SMK'	, 4);
	
	define('PRIVILEGE_SUPERUSER'	, 0);
	define('PRIVILEGE_SUPERVISOR'	, 2);
	define('PRIVILEGE_OPERATOR'		, 3);
	
	class Application_Base extends CI_Controller
	{
		var $data = array();
		
		var $file = array(
			'header' => 'home/header',
			'nav' => 'home/side_nav',
			'content' => null,
			'footer' => 'home/footer'
		);
		
		protected $institutionId = 0;
		protected $cityId = 0;
		protected $cityValue = '';
		
		public function __construct()
		{
			parent::__construct();
			
			$this->load->library('session');
			$this->load->model('M_Application');
			$this->load->model('M_Login');
			
			$institution = $this->M_Application->get_institution();
			$this->institutionId = $institution['id'];
			$this->cityId = $institution['city_id'];
			$this->cityValue = $institution['kota_asal_sek'];
			$this->isLogin();
		}
		
		public function isLogin()
		{
			$url = explode('/', current_url());
			
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$privilege = (isset($session['privilege']) ? $session['privilege'] : '');
			$cluster = (isset($session['cluster']) ? $session['cluster'] : '');
			$cluster = (substr($cluster, 0, 1) ? substr($cluster, 0, 1) : 0);
			
			if(!$this->M_Login->is_login($username))
			{
				redirect(site_url('login'));
			}
			
			if($privilege == PRIVILEGE_OPERATOR)
			{
				if(in_array('koreksi', $url))
				{
					redirect(site_url('login'));
				}
			}
			
			if($cluster == CLUSTER_SD)
			{
				if(in_array('smp', $url) || in_array('sma', $url) || in_array('smk', $url))
				{
					redirect(site_url('login'));
				}
			}
			else if($cluster == CLUSTER_SMP)
			{
				if(in_array('sd', $url) || in_array('sma', $url) || in_array('smk', $url))
				{
					redirect(site_url('login'));
				}
			}
			else if($cluster == CLUSTER_SMA)
			{
				if(in_array('sd', $url) || in_array('smp', $url) || in_array('smk', $url))
				{
					redirect(site_url('login'));
				}
			}
			else if($cluster == CLUSTER_SMK)
			{
				if(in_array('sd', $url) || in_array('smp', $url) || in_array('sma', $url))
				{
					redirect(site_url('login'));
				}
			}
		}
		
		function load_view($content = '', $data = '')
		{
			$this->file['content'] = ($content ? $content : '');
			$this->data = ($data ? $data : array());
			
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$operatorName = (isset($session['operator_name']) ? $session['operator_name'] : '');
			$privilege = (isset($session['privilege']) ? $session['privilege'] : '');
			$cluster = (isset($session['cluster']) ? $session['cluster'] : '');
			
			$authority = array(
				'privilege' => ($privilege ? $privilege : 0),
				'cluster' => (substr($cluster, 0, 1) ? substr($cluster, 0, 1) : 0)
			);
			
			$this->load->view($this->file['header'], array('operator_name' => $operatorName, 'privilege' => $privilege));
			$this->load->view($this->file['nav'], $authority);
			if($content) { $this->load->view($this->file['content'], $this->data); }
			$this->load->view($this->file['footer']);
		}
	}
?>