<?php

	class M_Application extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		
		public function get_institution()
		{
			$sql = "SELECT `id`, `city_id`, `kota_asal_sek`
				FROM `institution`
				LIMIT 1";
				
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}
	}
?>