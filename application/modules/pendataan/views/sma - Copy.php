<?php
	$count = count($this->data['list_sekolah']);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEAD-->
		
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE BREADCRUMB -->
		
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE BASE CONTENT -->
	   
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet light bordered">
					<div class="alert alert-block alert-success fade in" <?php echo (!isset($notification) ? 'hidden' : '') ?>>
						<button type="button" class="close" data-dismiss="alert"></button>
						<p><?php echo (isset($notification) ? $notification : ''); ?></p>
					</div>
					<div class="alert alert-block alert-danger fade in" <?php echo (!isset($error) ? 'hidden' : '') ?>>
						<button type="button" class="close" data-dismiss="alert"></button>
						<p><?php echo (isset($error) ? $error : ''); ?></p>
					</div>
					<div class="portlet-title">
						<div class="caption font-green-haze">
							<span class="caption-subject bold uppercase">Formulir Pendaftaran Siswa</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" method="post" action="<?php echo site_url('pendataan/sma'); ?>">
							<div class="form-body">
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Pencarian</label>
									<div class="col-md-8">
										<div class="input-group input-group-sm">
											<div class="input-group-control">
												<input type="text" name="no_ujian" class="form-control input-sm"
													placeholder="(Input No. Ujian) Format : *-**-**-**-***-***-*" value="<?php echo checkNull($this->data['siswa'], 'no_ujian') ?>">
												<div class="form-control-focus"> </div>
											</div>
											<span class="input-group-btn btn-right">
												<button class="btn green-haze" type="submit" name="form_pendataan" value="cari_siswa">Cari Siswa</button>
											</span>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-body">
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">No Daftar</label>
									<div class="col-md-4">
										<input type="text" name="no_daftar" value="<?php echo checkNull($this->data['siswa'], 'no_daftar') ?>" class="form-control"  id="form_control_1" placeholder="No Daftar" readonly>
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Alamat Rumah</label>
									<div class="col-md-4">
										<input type="text" name="alamat" value="<?php echo checkNull($this->data['siswa'], 'alamat'); ?>" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Nama Siswa</label>
									<div class="col-md-4">
										<input type="text" name="nama" value="<?php echo checkNull($this->data['siswa'], 'nama'); ?>" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
										<?php
											$kota = checkNull($this->data['institution'], 'kota');
											$kotaSiswa = checkNull($this->data['siswa'], 'kota');
											$flag = ($kota == '');
										?>
									<div class="col-md-4">
										<?php 
											if ($flag) {
												echo(
													'<input type="text" name="kota" class="form-control" id="form_control_1" placeholder="">
													<div class="form-control-focus"> </div>'
												);
											}else{
												echo(
													'<div class="md-radio-inline">
														<div class="md-radio">
															<input type="radio" name="kota" id="radio55" class="md-radiobtn" value="' . $kota . '" ' . ($kota == $kotaSiswa ? 'checked' : '') . '>
															<label for="radio55">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> ' . $kota . '
															</label>
														</div>
														<div class="md-radio">
															<input type="radio" name="kota" id="radio56" class="md-radiobtn" ' . ($kota != $kotaSiswa ? 'checked' : '') . '>
															<label for="radio56">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> Lainnya </label>
														</div>
													</div>'
												);
											}
										?>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Jenis Kelamin</label>
									<div class="col-md-4">
										<div class="md-radio-inline">
											<div class="md-radio">
												<input type="radio" name="jenis_kel" id="radio53" class="md-radiobtn" value="L" <?php echo (checkNull($this->data['siswa'], 'jenis_kel') == 'L' ? 'checked' : ''); ?>>
												<label for="radio53">
													<span></span>
													<span class="check"></span>
													<span class="box" ></span> Laki-laki </label>
											</div>
											<div class="md-radio">
												<input type="radio" name="jenis_kel" id="radio54" class="md-radiobtn" value="P" <?php echo (checkNull($this->data['siswa'], 'jenis_kel') == 'P' ? 'checked' : ''); ?>>
												<label for="radio54">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Perempuan </label>
											</div>
										</div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">No. Telpon</label>
									<div class="col-md-4">
										<input type="text" name="no_telp" value="<?php echo checkNull($this->data['siswa'], 'no_telp'); ?>" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Tempat Lahir</label>
									<div class="col-md-4">
										<input type="text" name="tmp_lahir" value="<?php echo checkNull($this->data['siswa'], 'tmp_lahir'); ?>" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Nama Orang Tua</label>
									<div class="col-md-4">
										<input type="text" name="nama_ortu" value="<?php echo checkNull($this->data['siswa'], 'nama_ortu'); ?>" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="control-label col-md-2">Tanggal Lahir</label>
									<div class="col-md-4">
										<input class="form-control form-control-inline input-medium date-picker" name="tgl_lahir" value="<?php echo checkNull($this->data['siswa'], 'tgl_lahir'); ?>" size="16" type="text"/>
										<span class="help-block"> Masukkan Tanggal Lahir </span>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 1</label>
									<div class="col-md-4">
										<select name="pilihan1" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
											<?php
												for($index = 0;$index<$count;$index++) {
													echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'">'. $this->data['list_sekolah'][$index]['nama'] .'</option>');
												}
											?>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Asal Sekolah</label>
									<div class="col-md-4">
										<input type="text" name="asal_sek" value="<?php echo checkNull($this->data['institution'], 'asal_sek'); ?>" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 2</label>
									<div class="col-md-4">
										<select name="pilihan2" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
									<?php
										$kota_asal_sek = checkNull($this->data['institution'], 'kota');
										$kota_asal_sek_siswa = checkNull($this->data['siswa'], 'kota');
										$flag = ($kota_asal_sek == '');
									?>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
									<div class="col-md-4">
										<?php 
											if ($flag) {
												echo(
													'<input type="text" name="kota_asal_sek" class="form-control" id="form_control_1" placeholder="">
													<div class="form-control-focus"> </div>'
												);
											}else{
												echo(
													'<div class="md-radio-inline">
														<div class="md-radio">
															<input type="radio" name="kota_asal_sek" id="radio57" class="md-radiobtn" value="'.$kota_asal_sek.'" ' . ($kota_asal_sek == $kota_asal_sek_siswa ? 'checked' : '') . '>
															<label for="radio57">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> ' . $kota_asal_sek . '
															</label>
														</div>
														<div class="md-radio">
															<input type="radio" name="kota_asal_sek" id="radio58" class="md-radiobtn" value="" ' . ($kota_asal_sek != $kota_asal_sek_siswa ? 'checked' : '') . '>
															<label for="radio58">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> Lainnya </label>
														</div>
													</div>'
												);
											}
										?>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 3</label>
									<div class="col-md-4">
										<select name="pilihan3" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN Bhs. Inggris</label>
									<div class="col-md-4">
										<input type="text" name="nilunbing" value="<?php echo checkNull($this->data['siswa'], 'nilunbing'); ?>" class="form-control"  id="form_control_1" placeholder="" readonly>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN Matematika</label>
									<div class="col-md-4">
										<input type="text" name="nilunmat" value="<?php echo checkNull($this->data['siswa'], 'nilunmat'); ?>" class="form-control"  id="form_control_1" placeholder="" readonly>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN IPA</label>
									<div class="col-md-4">
										<input type="text" name="nilunipa" value="<?php echo checkNull($this->data['siswa'], 'nilunipa'); ?>" class="form-control"  id="form_control_1" placeholder="" readonly>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">UN Bahasa Indonesia</label>
									<div class="col-md-4">
										<input type="text" name="nilunbind" value="<?php echo checkNull($this->data['siswa'], 'nilunbind'); ?>" class="form-control"  id="form_control_1" placeholder="" readonly>
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-12">
										<button type="submit" class="btn blue" name="form_pendataan" value="daftar_cetak_siswa">Tambah dan Print</button>
										<button type="submit" class="btn blue" name="form_pendataan" value="daftar_siswa" >Tambah Siswa</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		
		<!-- END PAGE BASE CONTENT -->
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php $this->load->view('javascript_pendataan'); ?>
<script type="text/javascript">
	$(".input-sm").mask("9-99-99-99-999-999-9");
</script>