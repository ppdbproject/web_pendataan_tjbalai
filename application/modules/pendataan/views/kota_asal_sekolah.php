<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
<div class="col-md-4">
	<?php 
		if ($flag) {
			echo(
				'<input type="text" name="kota_asal_sek" class="form-control" id="form_control_1" placeholder="">
				<div class="form-control-focus"> </div>'
			);
		}else{
			echo(
				'<div class="md-radio-inline">
					<div class="md-radio">
						<input type="radio" name="kota_asal_sek" id="radio57" class="md-radiobtn" value="'.$kota.'" ' . ($kota == $kotaSekolahSiswa ? 'checked' : '') . '>
						<label for="radio57">
							<span></span>
							<span class="check"></span>
							<span class="box"></span> ' . $kota . '
						</label>
					</div>
					<div class="md-radio">
						<input type="radio" name="kota_asal_sek" id="radio58" class="md-radiobtn" value="" ' . ($kota != $kotaSekolahSiswa ? 'checked' : '') . '>
						<label for="radio58">
							<span></span>
							<span class="check"></span>
							<span class="box"></span>
							<input type="text" name="input_kota_asal_sek" value="' . ($kota != $kotaSekolahSiswa ? $kotaSekolahSiswa : '') . '" style="display: inline; height: 25px;" class="form-control" placeholder="Lainnya">
						</label>
					</div>
				</div>'
			);
		}
	?>
</div>