<label class="col-md-2 control-label" for="form_control_1">Pilihan 1</label>
<div class="col-md-4">
	<select name="pilihan1" class="form-control" id="form_control_1">
		<option value="">Pilih Sekolah</option>
		<?php
			for($index = 0;$index<$count;$index++) {
				echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'">'. $this->data['list_sekolah'][$index]['nama'] .'</option>');
			}
		?>
	</select>
	<div class="form-control-focus"> </div>
</div>