<?php
	$count = count($this->data['list_sekolah']);
	
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEAD-->
		
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE BREADCRUMB -->
		
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE BASE CONTENT -->
	   
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet light bordered">
					<div class="alert alert-block alert-success fade in" <?php echo (!isset($notification) ? 'hidden' : '') ?>>
						<button type="button" class="close" data-dismiss="alert"></button>
						<p><?php echo (isset($notification) ? $notification : ''); ?></p>
					</div>
					<div class="alert alert-block alert-danger fade in" <?php echo (!isset($error) ? 'hidden' : '') ?>>
						<button type="button" class="close" data-dismiss="alert"></button>
						<p><?php echo (isset($error) ? $error : ''); ?></p>
					</div>
					<div class="portlet-title">
						<div class="caption font-green-haze">
							<span class="caption-subject bold uppercase">Formulir Pendaftaran Siswa</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" method="post" action="<?php echo site_url('pendataan/sd'); ?>">
							<div class="form-body">
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">No Daftar</label>
									<div class="col-md-4">
										<input type="text" name="no_daftar" value="" class="form-control"  id="form_control_1" placeholder="No Daftar" readonly=>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">No Akta</label>
									<div class="col-md-4">
										<input type="text" name="no_akte" value="" class="form-control"  id="form_control_1" placeholder="No Akta">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Alamat Rumah</label>
									<div class="col-md-4">
										<input type="text" name="alamat" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Nama Siswa</label>
									<div class="col-md-4">
										<input type="text" name="nama" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
									<div class="col-md-4">
										<input type="text" name="kota" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Jenis Kelamin</label>
									<div class="col-md-4">
										<div class="md-radio-inline">
											<div class="md-radio">
												<input type="radio" name="jenis_kel" id="radio53" class="md-radiobtn" value="L">
												<label for="radio53">
													<span></span>
													<span class="check"></span>
													<span class="box" ></span> Laki-laki </label>
											</div>
											<div class="md-radio">
												<input type="radio" name="jenis_kel" id="radio54" class="md-radiobtn" value="P">
												<label for="radio54">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Perempuan </label>
											</div>
										</div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">No. Telpon</label>
									<div class="col-md-4">
										<input type="text" name="no_telp" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Tempat Lahir</label>
									<div class="col-md-4">
										<input type="text" name="tmp_lahir" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Nama Orang Tua</label>
									<div class="col-md-4">
										<input type="text" name="nama_ortu" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="control-label col-md-2">Tanggal Lahir</label>
									<div class="col-md-4">
										<input class="form-control form-control-inline input-medium date-picker" name="tgl_lahir" value="" size="16" type="text"/>
										<span class="help-block"> Masukkan Tanggal Lahir </span>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 1</label>
									<div class="col-md-4">
										<select name="pilihan1" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
											<?php
												for($index = 0;$index<$count;$index++) {
													echo('<option value="'. $this->data['list_sekolah'][$index]['k_sekolah'] .'">'. $this->data['list_sekolah'][$index]['nama'] .'</option>');
												}
											?>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Asal Sekolah</label>
									<div class="col-md-4">
										<input type="text" name="asal_sek" value="" class="form-control" 	 id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 2</label>
									<div class="col-md-4">
										<select name="pilihan2" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
									<div class="col-md-4">
										<input type="text" name="kota_asaL_sek" value="" class="form-control" id="form_control_1" placeholder="">
										<div class="form-control-focus"> </div>
									</div>
									<label class="col-md-2 control-label" for="form_control_1">Pilihan 3</label>
									<div class="col-md-4">
										<select name="pilihan3" class="form-control" id="form_control_1">
											<option value="">Pilih Sekolah</option>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-12">
										<button type="button" class="btn blue" name="form_pendataan" value="daftar_cetak_siswa">Tambah dan Print</button>
										<button type="submit" class="btn blue" name="form_pendataan" value="daftar_siswa" >Tambah Siswa</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		
		<!-- END PAGE BASE CONTENT -->
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php $this->load->view('javascript_pendataan'); ?>