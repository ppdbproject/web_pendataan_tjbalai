<label class="control-label col-md-2">Tanggal Lahir</label>
<div class="col-md-4">
	<input class="form-control form-control-inline input-medium date-picker" name="tgl_lahir" value="<?php echo checkNull($this->data['siswa'], 'tgl_lahir'); ?>" size="16" type="text"/>
	<span class="help-block"> Masukkan Tanggal Lahir </span>
</div>