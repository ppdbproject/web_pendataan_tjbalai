<script>

	$(".input-sm").mask("9-99-99-99-999-999-9");

	function clearDataSiswa(){
		$('input[name=no_ujian]').val('');
		$('input[name=alamat_rumah]').val('');
		$('input[name=nama_siswa]').val('');
		$('input[name=kota_tempat_tinggal]').attr('checked', false);
		$('input[name=jenis_kelamin]').attr('checked', false);
		$('input[name=no_telepon]').val('');
		$('input[name=tempat_lahir]').val('');
		$('input[name=nama_orang_tua]').val('');
		$('input[name=tanggal_lahir]').val('');
		$('input[name=asal_sekolah]').val('');
		$('input[name=kota_sekolah]').attr('checked', false);
		$('input[name=un_bahasa_inggris]').val('');
		$('input[name=un_matematika]').val('');
		$('input[name=un_ipa]').val('');
		$('input[name=un_bahasa_indonesia]').val('');
	}
	var pilihan1 = '', pilihan2 = '', pilihan3 = ''
	$('select[name="pilihan1"]').on('focus', function(){
		var strNow = this.value;
	    $('select[name="pilihan1"]').change(function(){
			if ( $(this).val() !== strNow ) {
				$('select[name="pilihan2"],select[name="pilihan3"],select[name="pilihan4"]').empty();
				$('select[name="pilihan2"],select[name="pilihan3"],select[name="pilihan4"]').append('<option value="">Pilih Sekolah</option>');
				if($('select[name="jurusan1"]').length){
					$('select[name="jurusan1"],select[name="jurusan2"],select[name="jurusan3"]').empty();
					$('select[name="jurusan1"],select[name="jurusan2"],select[name="jurusan3"]').append('<option value="">Pilih Jurusan</option>');
				}
			}
			if  ($(this).val() !== '') {
				var str;
				if ( $('select[name="jurusan1"]').length ) {
					str = $("<select />").append(($(this).children(':not(:first-child)')).clone()).html();
				}
				else {
					str = $("<select />").append(($(this).children(':not(:first-child):not(option:selected)')).clone()).html();
				}
				$('select[name="pilihan2"]').append(str);
				strNow = this.value;
			}
			pilihan1 = strNow;
		})
	});

	$('select[name="pilihan2"]').on('focus', function(){
		var strNow = this.value;
	    $('select[name="pilihan2"]').change(function(){
			if ( $(this).val() !== strNow ) {
				$('select[name="pilihan3"]').empty();
				$('select[name="pilihan3"]').append('<option value="">Pilih Sekolah</option>')
			}
			if  ($(this).val() !== '') {
				var str;
				if ( $('select[name="jurusan1"]').length && pilihan1 != $(this).val()) {
					str = $("<select />").append(($(this).children(':not(:first-child)')).clone()).html();
				}
				else{
					str = $("<select />").append(($(this).children(':not(:first-child):not(option:selected)')).clone()).html();
				}
				$('select[name="pilihan3"]').append(str);
				strNow = this.value;
			}
			pilihan2 = strNow;
		})
	});

	$('select[name="pilihan3"]').on('focus', function(){
		var strNow = this.value;
	    $('select[name="pilihan3"]').change(function(){
			if ( $(this).val() !== strNow ) {
				$('select[name="pilihan4"]').empty();
				$('select[name="pilihan4"]').append('<option value="">Pilih Sekolah</option>')
			}
			if  ($(this).val() !== '') {
				var str;
				if ( $('select[name="jurusan1"]').length && pilihan1 != $(this).val()) {
					str = $("<select />").append(($(this).children(':not(:first-child)')).clone()).html();
				}
				else{
					str = $("<select />").append(($(this).children(':not(:first-child):not(option:selected)')).clone()).html();
				}
				$('select[name="pilihan4"]').append(str);
				strNow = this.value;
			}
			pilihan2 = strNow;
		})
	});

	$(document).ready(function(){
		if ( $('select[name="jurusan1"]').length ) {
			$('select[name="pilihan1"]').change(function(){
				if ( this.value != '' ) 
				{
					$('select[name="jurusan1"]').removeAttr('disabled');
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('pendataan/jurusan'); ?>",
						data: { pilihan: $('select[name="pilihan1"]').val() },
						datatype: "json",
						// cache:false,
						success:
							function(data){
								var json = jQuery.parseJSON(data);
								$('select[name="jurusan1"]').empty();
								$('select[name="jurusan1"]').append('<option value="">Pilih Jurusan</option>');
								for (var i = 0; i < json.length; i++) {
									$('select[name="jurusan1"]').append(
										'<option value="'+json[i]['k_jurusan']+'">'+json[i]['nama']+'</option>'
									);
								}
							}
					});
				}
				else 
				{
					$('select[name=jurusan1]').attr('disabled','');
				}
				$('select[name=jurusan2]').attr('disabled','');
				$('select[name=jurusan3]').attr('disabled','');
			})

			$('select[name=pilihan1],select[name=jurusan1]').change(function(){
				if ( $('select[name=pilihan1]').val() != '' && $('select[name=jurusan1]').val() != '' ) 
				{
					$('select[name="jurusan2"]').removeAttr('disabled');
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('pendataan/jurusan'); ?>",
						data: { pilihan: $('select[name="pilihan1"]').val() },
						datatype: "json",
						// cache:false,
						success:
							function(data){
								var json = jQuery.parseJSON(data);
								$('select[name="jurusan2"]').empty();
								$('select[name="jurusan2"]').append('<option value="">Pilih Jurusan</option>');
								for (var i = 0; i < json.length; i++) {
									if(
										$('select[name=jurusan1]').val() != json[i]['k_jurusan']
									){
										$('select[name="jurusan2"]').append(
											'<option value="'+json[i]['k_jurusan']+'">'+json[i]['nama']+'</option>'
										);
									}
								}
							}
					});
				}
				else 
				{
					$('select[name=jurusan2],select[name="jurusan3]').attr('disabled','');
					$('select[name=jurusan2],select[name="jurusan3]').empty();
					$('select[name=jurusan2],select[name="jurusan3]').append('<option value="">Pilih Jurusan</option>');
				}
			})

			$('select[name="pilihan1"],select[name="jurusan2"]').change(function(){
				if ( $('select[name=pilihan1]').val() != '' && $('select[name=jurusan2]').val() != '')
				{
					$('select[name="jurusan3"]').removeAttr('disabled');
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('pendataan/jurusan'); ?>",
						data: { pilihan: $('select[name="pilihan1"]').val() },
						datatype: "json",
						// cache:false,
						success: 
							function(data){
								var json = jQuery.parseJSON(data);
								$('select[name="jurusan3"]').empty();
								$('select[name="jurusan3"]').append('<option value="">Pilih Jurusan</option>');
								for (var i = 0; i < json.length; i++) {
									if(
										$('select[name=jurusan1]').val() != json[i]['k_jurusan'] &&
										$('select[name=jurusan2]').val() != json[i]['k_jurusan']
									){
										$('select[name="jurusan3"]').append(
											'<option value="'+json[i]['k_jurusan']+'">'+json[i]['nama']+'</option>'
										);
									}
								}
							}
					});
				}
				else 
				{
					$('select[name=jurusan3]').attr('disabled','');
				}
			})
		}
	})
</script>