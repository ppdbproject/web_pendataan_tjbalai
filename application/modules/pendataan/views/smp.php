<?php
	$count = count($this->data['list_sekolah']);
	$kota = checkNull($this->data['institution'], 'kota');
	$kotaSiswa = checkNull($this->data['siswa'], 'kota');
	$kotaSekolahSiswa = checkNull($this->data['siswa'], 'kota_asal_sek');
	$flag = ($kota == '');
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEAD-->
		
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE BREADCRUMB -->
		
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE BASE CONTENT -->
	   
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet light bordered">
					<?php include('notification.php') ?>
					<div class="portlet-title">
						<div class="caption font-green-haze">
							<span class="caption-subject bold uppercase">Formulir Pendaftaran Siswa</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" method="post" action="<?php echo site_url('pendataan/smp'); ?>">
							<?php include('search.php') ?>
							<hr />
							<div class="form-body">
								<div class="form-group form-md-line-input">
									<?php include('nama.php') ?>
									<?php include('alamat.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('jenis_kelamin.php') ?>
									<?php include('kota.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('tempat_lahir.php') ?>
									<?php include('no_telp.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('tgl_lahir.php') ?>
									<?php include('nama_ortu.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('asal_sekolah.php') ?>
									<?php include('pilihan1.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('kota_asal_sekolah.php') ?>
									<?php include('pilihan2.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('un_bhs_ind.php') ?>
									<?php include('pilihan3.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('un_mat.php') ?>
									<?php // include('pilihan4.php') ?>
								</div>
								<div class="form-group form-md-line-input">
									<?php include('un_ipa.php') ?>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-12">
										<button type="submit" class="btn blue" name="form_pendataan" value="daftar_cetak_siswa">Tambah dan Print</button>
										<button type="submit" class="btn blue" name="form_pendataan" value="daftar_siswa" >Tambah Siswa</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
				<!-- BEGIN SAMPLE FORM PORTLET-->
				
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>
		
		<!-- END PAGE BASE CONTENT -->
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include('javascript_pendataan.php'); ?>