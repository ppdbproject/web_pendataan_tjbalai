<label class="col-md-2 control-label" for="form_control_1">Jenis Kelamin</label>
<div class="col-md-4">
	<div class="md-radio-inline">
		<div class="md-radio">
			<input type="radio" name="jenis_kel" id="radio53" class="md-radiobtn" value="L" <?php echo (checkNull($this->data['siswa'], 'jenis_kel') == 'L' ? 'checked' : ''); ?>>
			<label for="radio53">
				<span></span>
				<span class="check"></span>
				<span class="box" ></span> Laki-laki </label>
		</div>
		<div class="md-radio">
			<input type="radio" name="jenis_kel" id="radio54" class="md-radiobtn" value="P" <?php echo (checkNull($this->data['siswa'], 'jenis_kel') == 'P' ? 'checked' : ''); ?>>
			<label for="radio54">
				<span></span>
				<span class="check"></span>
				<span class="box"></span> Perempuan </label>
		</div>
	</div>
</div>