<label class="col-md-2 control-label" for="form_control_1">Kota / Kab. </label>
<div class="col-md-4">
	<?php 
		if ($flag) {
			echo(
				'<input type="text" name="kota" class="form-control" id="form_control_1" placeholder="">
				<div class="form-control-focus"> </div>'
			);
		}else{
			echo(
				'<div class="md-radio-inline">
					<div class="md-radio">
						<input type="radio" name="kota" id="radio55" class="md-radiobtn" value="' . $kota . '" ' . ($kota == $kotaSiswa ? 'checked' : '') . '>
						<label for="radio55">
							<span></span>
							<span class="check"></span>
							<span class="box"></span> ' . $kota . '
						</label>
					</div>
					<div class="md-radio">
						<input type="radio" name="kota" id="radio56" class="md-radiobtn" value="" ' . ($kota != $kotaSiswa ? 'checked' : '') . '>
						<label for="radio56">
							<span></span>
							<span class="check"></span>
							<span class="box"></span>
							<input type="text" name="input_kota" value="' . ($kota != $kotaSiswa ? $kotaSiswa : '') . '" style="display: inline; height: 25px;" class="form-control" placeholder="Lainnya">
						</label>
					</div>
				</div>'
			);
		}
	?>
</div>