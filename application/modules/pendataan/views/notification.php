<div class="alert alert-block alert-success fade in" <?php echo (!isset($notification) || !$notification ? 'hidden' : '') ?>>
	<button type="button" class="close" data-dismiss="alert"></button>
	<p><?php echo (isset($notification) ? $notification : ''); ?></p>
</div>
<div class="alert alert-block alert-danger fade in" <?php echo (!isset($error) || !$error ? 'hidden' : '') ?>>
	<button type="button" class="close" data-dismiss="alert"></button>
	<p><?php echo (isset($error) ? $error : ''); ?></p>
</div>