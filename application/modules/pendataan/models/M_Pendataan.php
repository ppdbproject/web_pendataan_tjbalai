<?php
	class M_Pendataan extends CI_Model {
		function __construct(){
			parent::__construct();
		}

		function get_data_siswa($noUjian, $tableName) {
			$sql = "SELECT * FROM $tableName
				WHERE `no_ujian` LIKE '$noUjian'";				
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_siswa_sd($noAkte) {
			$sql = "SELECT * FROM `pendaftar_sd`
				WHERE `no_akte` LIKE '$noAkte'";

			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_data_sekolah($username) {
			$sql = "SELECT * FROM `m_sekolah`
				WHERE `k_sekolah` LIKE CONCAT(SUBSTRING((
					SELECT `k_sekolah` FROM `m_operator`
					WHERE `opr_id` LIKE '" .  $username . "'
				),1,1),'%')";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->result_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_list_sekolah($cluster) {
			$sql = "SELECT * FROM `m_sekolah`
				WHERE SUBSTRING(`k_sekolah`, 1, 1) = $cluster";
				
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->result_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_all_data_sekolah() {
			$sql = "SELECT * FROM `m_sekolah`";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->result_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function set_daftar($tableName, $data) {
			return $this->db->insert($tableName, $data);
		}
		
		function set_pilihan($tableName, $data) {
			return $this->db->insert($tableName, $data);
		}
		
		function update_counter($lokasi) {
			$sql = "UPDATE `counter` SET `jml_pendaftar` = `jml_pendaftar` + 1
				WHERE `k_lokasi` = $lokasi";
				
			$this->db->query($sql);
		}
		
		function generate_no_urut_pendaftaran($lokasi)
		{
			$sql = "SELECT jml_pendaftar
				FROM `counter`
				WHERE k_lokasi = $lokasi";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}
		
		function get_kode_pendaftaran()
		{
			$sql = "SELECT b.kd_prop, b.id 
				FROM institution a
				LEFT JOIN city b ON b.id = a.city_id
				WHERE a.id = 1";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_list_jurusan($kdSekolah)
		{
			$sql = "SELECT * 
				FROM jurusan_smk
				WHERE k_sekolah LIKE $kdSekolah";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = json_encode($query->result_array(), true);
				$query->free_result();
				return $data;
			}else {
				return json_encode(array(), true);
			}
		}
	}
		
?>