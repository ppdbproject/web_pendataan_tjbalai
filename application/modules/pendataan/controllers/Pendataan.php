<?php
	require_once( APPPATH . 'modules/application_base/controllers/Application_Base.php' );
	require_once( APPPATH . 'modules/pendataan/controllers/features.php' );
	
	class Pendataan extends Application_Base
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->load->library('session');
			
			$this->load->model('M_Pendataan');
			$this->load->model('M_Login');
			$this->load->model('M_Cetak');
		}

		public function sd()
		{
			$this->pendataanProcess(CLUSTER_SD);
		}
		
		public function smp()
		{
			$this->pendataanProcess(CLUSTER_SMP);
		}
		
		public function sma()
		{
			$this->pendataanProcess(CLUSTER_SMA);
		}
		
		public function smk()
		{
			$this->pendataanProcess(CLUSTER_SMK);
		}
		
		public function generateNoPendaftaran($lokasi)
		{
			$count = $this->M_Pendataan->generate_no_urut_pendaftaran($lokasi);
			$noUrutPendaftaran = sprintf("%04d", $count['jml_pendaftar'] + 1);
			$kodePendaftaran = $this->M_Pendataan->get_kode_pendaftaran();
			$noPendaftaran = date('y') . '-' . sprintf("%03d", $kodePendaftaran['id']) . '-' . $lokasi . '-' . $noUrutPendaftaran;
			
			return $noPendaftaran;
		}
		
		public function getDataSiswa($dataMaster, $cluster)
		{
			$data = array(
				'alamat' => $this->input->post('alamat'),
				'nama' => $this->input->post('nama'),
				'kota' => ($this->input->post('kota') ? $this->input->post('kota') : $this->input->post('input_kota')),
				'jenis_kel' => $this->input->post('jenis_kel'),
				'no_telp' => $this->input->post('no_telp'),
				'tmp_lahir' => $this->input->post('tmp_lahir'),
				'nama_ortu' => $this->input->post('nama_ortu'),
				'tgl_lahir' => $this->input->post('tgl_lahir')
			);
			
			if($cluster == CLUSTER_SD)
			{
				$data['no_akte'] = $this->input->post('no_akte');
				$data['asal_sek'] = $this->input->post('asal_sek');
				$data['kota_asal_sek'] = ($this->input->post('kota_asal_sek') ? $this->input->post('kota_asal_sek') : $this->input->post('input_kota_asal_sek'));
			}
			else
			{
				if($cluster == CLUSTER_SMA || $cluster == CLUSTER_SMK)
				{
					$data['nilunbing'] = $dataMaster['nilunbing'];
					$data['nilaiakhir'] = $dataMaster['nilunbing'] + $dataMaster['nilunmat'] +
						$dataMaster['nilunipa'] + $dataMaster['nilunbind'];
				}
				else
				{
					$data['nilaiakhir'] = $dataMaster['nilunmat'] + $dataMaster['nilunipa'] + $dataMaster['nilunbind'];
				}
				$data['no_ujian'] = $dataMaster['no_ujian'];
				$data['asal_sek'] = $dataMaster['asal_sek'];
				$data['kota_asal_sek'] = $dataMaster['kota_asal_sek'];
				$data['nilunmat'] = $dataMaster['nilunmat'];
				$data['nilunipa'] = $dataMaster['nilunipa'];
				$data['nilunbind'] = $dataMaster['nilunbind'];
				
			}
			
			return $data;
		}
		
		public function getPilihan($cluster, $nilaiAkhir = '')
		{
			for($index = 1; $index <= 4; $index++)
			{
				$data['pilihan' . $index] = array(
					'pilih_urut' => $index,
					'pilih_sek' => ($cluster != CLUSTER_SMK ? $this->input->post('pilihan' . $index) : $this->input->post('jurusan' . $index))
				);
				
				if($cluster != CLUSTER_SD)
				{
					$data['pilihan' . $index]['nilaiakhir'] = $nilaiAkhir;
				}
			}
			
			return $data;
		}
		
		public function getClusterPendaftar($cluster)
		{
			switch($cluster)
			{
				case CLUSTER_SD :
					$result = array(
						'tabel_pendaftar' => 'pendaftar_sd',
						'tabel_master' => 'master_sd'
					);
					return $result;
					
				case CLUSTER_SMP :
					$result = array(
						'tabel_pendaftar' => 'pendaftar_smp',
						'tabel_master' => 'master_smp'
					);
					return $result;
					
				case CLUSTER_SMA :
					$result = array(
						'tabel_pendaftar' => 'pendaftar_sma',
						'tabel_master' => 'master_sma_smk'
					);
					return $result;
					
				case CLUSTER_SMK :
					$result = array(
						'tabel_pendaftar' => 'pendaftar_smk',
						'tabel_master' => 'master_sma_smk'
					);
					return $result;
			}
		}
		
		private function search($clusterPendaftar, $noUjian)
		{
			if($noUjian)
			{
				$data['siswa'] = $this->M_Pendataan->get_data_siswa($noUjian, $clusterPendaftar['tabel_pendaftar']);
				if(!$data['siswa'])
				{
					$data['siswa'] = $this->M_Pendataan->get_data_siswa($noUjian, $clusterPendaftar['tabel_master']);
					if(!$data['siswa'])
					{
						$data['error'] = 'Nomor Ujian tidak ada dalam data siswa';
					}
				}
				else
				{
					$data['error'] = 'Siswa telah melakukan pendaftaran dengan Nomor Pendaftaran ' . $data['siswa']['no_daftar'];
					$data['siswa'] = array();
				}
			}
			else
			{
				$data['error'] = 'Nomor ujian perlu diisi dahulu';
			}
			
			return $data;
		}
		
		public function daftarSiswa($cluster, $tabelPendaftar, $tabelPilihan)
		{
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$authority = $this->M_Login->get_authority($username);
			
			if($cluster == CLUSTER_SD)
			{
				$data['siswa'] = $this->getDataSiswa(array(), $cluster);
				$pilihan = $this->getPilihan($cluster);
				if(!$pilihan['pilihan1']['pilih_sek'])
				{
					$data['error'] = 'Pilihan 1 tidak boleh kosong';
				}
				else if(
					$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan2']['pilih_sek'] || 
					$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan3']['pilih_sek'] ||
					($pilihan['pilihan2']['pilih_sek'] == $pilihan['pilihan3']['pilih_sek'] &&
					$pilihan['pilihan2']['pilih_sek'] != '')							
				){
					$data['error'] = 'Pilihan Sekolah Tidak Boleh Sama';
				}
				else
				{
					$dataSiswa = $this->M_Pendataan->get_siswa_sd($data['siswa']['no_akte'], 'pendaftar_sd');
					if(!$dataSiswa)
					{
						$data['siswa']['opr_id'] = $username;
						$data['siswa']['no_daftar'] = $this->generateNoPendaftaran($authority['k_sekolah']);
						$data['siswa']['k_lokasi'] = $authority['k_sekolah'];
						if($this->M_Pendataan->set_daftar('pendaftar_sd', $data['siswa']))
						{
							$this->M_Pendataan->update_counter($session['cluster']);
							for($index = 1; $index <= count($pilihan); $index++)
							{
								if($pilihan['pilihan' . $index]['pilih_sek'])
								{
									$pilihan['pilihan' . $index]['no_daftar'] = $data['siswa']['no_daftar'];
									if($pilihan['pilihan' . $index]['pilih_sek']){
										if($this->M_Pendataan->set_pilihan($tabelPilihan, $pilihan['pilihan' . $index]))
										{
											$data['notification'] = 'Siswa berhasil didaftarkan';
										}
										else
										{
											$data['notification'] = '';
											$data['error'] = 'Siswa gagal didaftarkan';
											$index = count($pilihan) + 1;
										}
									}
								}
							}
						}
					}
					else
					{
						$data['error'] = 'Siswa telah melakukan pendaftaran dengan Nomor Pendaftaran ' . $dataSiswa['no_daftar'];
					}
				}
			}
			else
			{
				$clusterPendaftar = $this->getClusterPendaftar($cluster);
				$noUjian = ($this->input->post('no_ujian') ? $this->input->post('no_ujian') : '');
				$dataSearch = $this->search($clusterPendaftar, $noUjian);
				$dataSiswa = (isset($dataSearch['siswa']) ? $dataSearch['siswa'] : array());
				
				if(isset($dataSiswa) && $dataSiswa){
					$data['siswa'] = $this->getDataSiswa($dataSiswa, $cluster);
					$data['siswa']['no_daftar'] = $this->generateNoPendaftaran($authority['k_sekolah']);
					$data['siswa']['k_lokasi'] = $authority['k_sekolah'];
					
					$pilihan = $this->getPilihan($cluster, $data['siswa']['nilaiakhir']);
					if(!$pilihan['pilihan1']['pilih_sek'])
					{
						$data['error'] = 'Pilihan 1 tidak boleh kosong';
					}
					else if(
						$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan2']['pilih_sek'] || 
						$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan3']['pilih_sek'] ||
						$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan4']['pilih_sek'] ||
						($pilihan['pilihan2']['pilih_sek'] == $pilihan['pilihan3']['pilih_sek'] &&
						$pilihan['pilihan2']['pilih_sek'] != '')||
						($pilihan['pilihan2']['pilih_sek'] == $pilihan['pilihan4']['pilih_sek'] &&
						$pilihan['pilihan2']['pilih_sek'] != '')||
						($pilihan['pilihan3']['pilih_sek'] == $pilihan['pilihan4']['pilih_sek'] &&
						$pilihan['pilihan4']['pilih_sek'] != '')
					){
						$data['error'] = 'Pilihan Sekolah Tidak Boleh Sama';
					}
					else
					{
						$data['siswa']['opr_id'] = $username;
						if($this->M_Pendataan->set_daftar($tabelPendaftar, $data['siswa']))
						{
							$this->M_Pendataan->update_counter($data['siswa']['k_lokasi']);
							for($index = 1; $index <= count($pilihan); $index++)
							{
								$pilihan['pilihan' . $index]['no_daftar'] = $data['siswa']['no_daftar'];
								if($pilihan['pilihan' . $index]['pilih_sek']){
									if($this->M_Pendataan->set_pilihan($tabelPilihan, $pilihan['pilihan' . $index]))
									{
										$data['notification'] = 'Siswa berhasil didaftarkan';
									}
									else
									{
										$data['notification'] = '';
										$data['error'] = 'Siswa gagal didaftarkan';
										$index = count($pilihan) + 1;
									}
								}
							}
						}
					}
				}
				else
				{
					$data['error'] = (isset($dataSearch['error']) ? $dataSearch['error'] : '');
					$data['notification'] = (isset($dataSearch['notification']) ? $dataSearch['notification'] : '');
				}
			}
			
			return $data;
		}
		
		public function pendataanProcess($cluster)
		{
			$this->load->model('cetak/M_Cetak');
			$session = $this->session->all_userdata();
			$username = (isset($session['username']) ? $session['username'] : '');
			$authority = $this->M_Login->get_authority($username);
			
			switch($cluster)
			{
				case CLUSTER_SD :
					$tabelPendaftar = 'pendaftar_sd';
					$tabelPilihan = 'pilihan_sd';
					$view = 'sd';
					break;
					
				case CLUSTER_SMP :
					$tabelPendaftar = 'pendaftar_smp';
					$tabelPilihan = 'pilihan_smp';
					$view = 'smp';
					break;
					
				case CLUSTER_SMA :
					$tabelPendaftar = 'pendaftar_sma';
					$tabelPilihan = 'pilihan_sma';
					$view = 'sma';
					break;
					
				case CLUSTER_SMK :
					$tabelPendaftar = 'pendaftar_smk';
					$tabelPilihan = 'pilihan_smk';
					$view = 'smk';
					break;
					
				default :
					$tabelPendaftar = '';
					$tabelPilihan = '';
					$view = '';
					break;
			}
			
			switch ($this->input->post('form_pendataan')) {
				case 'cari_siswa':
					$clusterPendaftar = $this->getClusterPendaftar($cluster);
					$noUjian = ($this->input->post('no_ujian') ? $this->input->post('no_ujian') : '');
					
					$data = $this->search($clusterPendaftar, $noUjian);
					$data['siswa']['opr_id'] = $username;
					
					break;

				case 'daftar_siswa':
					if(isset($_POST)){
						unset($_POST['form_pendataan']);
						$data = $this->daftarSiswa($cluster, $tabelPendaftar, $tabelPilihan);
						if(isset($data['notification']) && $data['notification'])
						{
							$data['siswa'] = array();
						}
					}
					break;
				
				case 'daftar_cetak_siswa' :
					if(isset($_POST)){
						unset($_POST['form_pendataan']);
						$data = $this->daftarSiswa($cluster, $tabelPendaftar, $tabelPilihan);
						
						$pilihan = $this->getPilihan($cluster);
						if($pilihan['pilihan1']['pilih_sek'] &&
							!(
								$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan2']['pilih_sek'] || 
								$pilihan['pilihan1']['pilih_sek'] == $pilihan['pilihan3']['pilih_sek'] ||
								($pilihan['pilihan2']['pilih_sek'] == $pilihan['pilihan3']['pilih_sek'] &&
								$pilihan['pilihan2']['pilih_sek'] != '')
							)
						){
							if(
								(isset($data['error']) && !$data['error']) ||
								(isset($data['notification']) && $data['notification'])
							)
							{
								$data['nama_institusi'] = $this->M_Cetak->get_nama_institusi();
								$data['data_sekolah'] = $this->M_Cetak->get_data_sekolah($username);
								$data['data_daftar'] = $this->M_Cetak->get_data_daftar($data['siswa']['no_daftar'], $authority['k_sekolah']);
								$data['nama_operator']['nama'] = $session['operator_name'];
								for($index = 0; $index < 4; $index++)
								{
									if($pilihan['pilihan' . ($index + 1)]['pilih_sek']){
										$tmp = $this->M_Cetak->get_nama_sekolah( substr($pilihan['pilihan' . ($index + 1)]['pilih_sek'], 0, 3) );
										// $tmp_nama = $tmp['nama'];
										$data['data_pilihan'][$index]['nama'] = $tmp['nama'];
									}
								}
								$view = 'cetak/Cetak';
							}
						}
					}
					break;
					
				default:
					# code...
					break;
			}
			
			if(substr($session['cluster'], 0, 1) == CLUSTER_ALL)
			{
				$data['list_sekolah'] = $this->M_Pendataan->get_list_sekolah($cluster);
			}
			else{
				$data['list_sekolah'] = $this->M_Pendataan->get_data_sekolah($username);
			}
			
			if (!isset($data['siswa'])) {
				$data['siswa'] = array();
			}
			
			$data['institution']['kota'] = $this->cityValue;
			
			$this->load_view($view, $data);
		}

		public function jurusan() {
			$message = '';
			if( isset($_POST['pilihan']) )
		    {
		        $message = $this->M_Pendataan->get_list_jurusan($_POST['pilihan']);
		    }
		    echo ($message);
		}
	}
?>