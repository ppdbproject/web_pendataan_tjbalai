<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class MY_Controller extends CI_Controller{
protected function render( $template = 'index', $data = '' ) {
		
    $this->load->helper( 'text' );
    $this->load->helper( 'general' );
    $this->load->model( 'users' );
    
    if (!$this->session->userdata('is_login'))
            redirect(base_url('login'));
    $this->load->view( 'header', $data );
//		

    $this->load->view( $template, $data );
    /* FETCH GALLERY */


    $this->load->view( 'footer', $data );
    if( isset( $this->db ) ) $this->db->close(); 
    }
    
    public function pagination_config($url = "", $total_rows = "", $limit = 15) {
        $config_page = array(
            'base_url' => $url,
            'total_rows' => $total_rows,
            'per_page' => $limit,
            'page_query_string'	=> TRUE,
            'use_page_numbers'=>TRUE,
            'full_tag_open' => '<ul class="pagination">',
            'full_tag_close' => '</ul>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a href="#">',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li class="next">',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li class="prev">',
            'prev_tag_close' => '<li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>'
        );
        return $config_page;
    }
}
?>
