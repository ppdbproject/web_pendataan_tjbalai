<?php
	class M_Login extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login_process($username, $password)
		{
			$sql = "SELECT `opr_id`, `nama`, `id_role`, `k_sekolah`, `is_blokir`
				FROM `m_operator`
				WHERE `opr_id` LIKE '$username' AND `passwd` LIKE '$password'";
				
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$result = $query->row_array();
				$query->free_result();
				return $result;
			}
			else
			{
				return array();
			}
		}
		
		public function is_login($username)
		{
			$sql = "SELECT `opr_id` FROM `m_operator`
				WHERE `opr_id` LIKE '$username'";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$query->free_result();
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function get_authority($username)
		{
			$sql = "SELECT `id_role`, `k_sekolah` FROM `m_operator`
				WHERE `opr_id` LIKE '$username'";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$result = $query->row_array();
				$query->free_result();
				return $result;
			}
			else
			{
				return array();
			}
		}

		public function get_op_name($username)
		{
			$sql = "SELECT `nama` FROM `m_operator`
				WHERE `opr_id` LIKE '$username'";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$result = $query->row_array();
				$query->free_result();
				return $result;
			}
			else
			{
				return array();
			}
		}
	}
?>