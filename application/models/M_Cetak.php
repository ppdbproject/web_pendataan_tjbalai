<?php

	class M_Cetak extends CI_Model {
		function __construct(){
			parent::__construct();
		}

		function get_nama_institusi() {
			$sql = "SELECT `name` FROM `institution`";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_data_sekolah($username) {
			$sql = "SELECT * FROM `m_sekolah`
				WHERE `k_sekolah` LIKE CONCAT((
					SELECT `k_sekolah` FROM `m_operator`
					WHERE `opr_id` LIKE '" .  $username . "'
				))";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_data_daftar($noDaftar,$kLokasi) {
			$cluster = ($kLokasi ? substr($kLokasi, 0, 1) : 0);
			
			if($cluster == 1){ $tablePendaftar = 'pendaftar_sd'; }
			else if($cluster == 2){ $tablePendaftar = 'pendaftar_smp'; }
			else if($cluster == 3){ $tablePendaftar = 'pendaftar_sma'; }
			else{ $tablePendaftar = 'pendaftar_smk'; }
			$sql = "SELECT * FROM $tablePendaftar
				WHERE `no_daftar` LIKE '$noDaftar' AND `k_lokasi` LIKE $kLokasi";

			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_nama_sekolah($kdSekolah) {
			$sql = "SELECT `nama` FROM `m_sekolah`
				WHERE `k_sekolah` LIKE '$kdSekolah' ";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->row_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}

		function get_pilihan($noDaftar, $tabelPilihan) {

			$sql = "SELECT `pilih_sek` FROM $tabelPilihan
				WHERE `no_daftar` LIKE '$noDaftar' ";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0 )
			{
				$data = $query->result_array();
				$query->free_result();
				return $data;
			}else{
				return array();
			}
		}
	}
		
?>